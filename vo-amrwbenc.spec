Name:			vo-amrwbenc
Version:		0.1.3
Release:		1
Summary:		VisualOn AMR-WB encoder library
License:		ASL 2.0
URL:			http://opencore-amr.sourceforge.net/
Source0:		http://downloads.sourceforge.net/opencore-amr/%{name}/%{name}-%{version}.tar.gz

BuildRequires: gcc

%description
This library contains an encoder implementation of the Adaptive 
Multi Rate Wideband (AMR-WB) audio codec. The library is based 
on a codec implementation by VisualOn as part of the Stagefright 
framework from the Google Android project.

%package        devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static
%make_build

%install
%make_install
rm %{buildroot}%{_libdir}/libvo-amrwbenc.la


%ldconfig_scriptlets


%files
%license COPYING
%doc README NOTICE
%{_libdir}/libvo-amrwbenc.so.*

%files devel
%{_includedir}/%{name}
%{_libdir}/libvo-amrwbenc.so
%{_libdir}/pkgconfig/vo-amrwbenc.pc

%changelog
* Fri May 07 2021 weidong <weidong@uniontech.com> - 0.1.3-1
- Initial package.
